
//Logout test (SeleniumWD & Node.js)

var webdriver = require('selenium-webdriver'),
    By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

var driver = new webdriver.Builder()
    .forBrowser('firefox')
    .build();

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

driver.get('https://google.com');
driver.findElement(By.id('gb_71')).click();

driver.findElement(By.id('gb_70')).then(function(element) {
  console.log('Logout successfully.');
}, function(error) {
  console.log('The element was not found, as expected');
});
driver.quit();
